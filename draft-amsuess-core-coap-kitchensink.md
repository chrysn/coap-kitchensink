---
title: "Everything over CoAP"
docname: draft-amsuess-core-coap-kitchensink-latest
ipr: trust200902
stand_alone: true
cat: info
wg: CoRE
kw: CoAP, NTP
author:
- ins: C. Amsüss
  name: Christian Amsüss
  country: Austria
  email: christian@amsuess.com
normative:
  RFC7252:
informative:
  gemini:
    title: Project Gemini, speculative specification, v0.16.1
    author:
      -
        ins: solderpunk
    date: 2022-01-30
    format:
      HTML: https://gemini.circumlunar.space/docs/specification.html
  CoJP: RFC9031
  ace-key-groupcomm-oscore: I-D.ietf-ace-key-groupcomm-oscore
  OpenThread Multicast:
    title: Thread Border Router – Thread 1.2 Multicast
    author:
      -
        ins: Simon Lin
    date: 2022-03-15
    format:
      HTML: https://openthread.io/codelabs/openthread-border-router-ipv6-multicast#0
  DHCP: RFC8415
  IGMP: RFC3376
  OpenVPNproxy:
    title: Connecting to an OpenVPN server via an HTTP proxy
    format:
      HTML: https://openvpn.net/community-resources/connecting-to-an-openvpn-server-via-an-http-proxy/
  XEP-0124:
    title: Bidirectional-streams Over Synchronous HTTP (BOSH)
    author:
      -
        ins: Ian Paterson
      -
        ins: Dave Smith
      -
        ins: Peter Saint-Andre
      -
        ins: Jack Moffitt
      -
        ins: Lance Stout
      -
        ins: Winfried Tilanus
    format:
      HTML: https://xmpp.org/extensions/xep-0124.html
  CoAP-over-NB-IoT:
    title: "Lightweight Machine to Machine Technical Specification: Transport Layer"
    version: "1.1"
    format:
      PDF: http://openmobilealliance.org/RELEASE/LightweightM2M/V1_1-20180612-C/OMA-TS-LightweightM2M_Transport-V1_1-20180612-C.pdf
    author:
    -
      ins: Open Mobile Alliance
  OHAI: RFC9458
  summit2023:
    title: Welcome to the RIOT Summit 2023
    author:
      -
        ins: Oleg Hahm
    format:
        PDF: http://summit.riot-os.org/2023/wp-content/uploads/sites/18/2023/09/2023-09-Summit-Welcome.pdf#page=7



--- abstract

The Constrained Application Protocol (CoAP) has become the base of applications
both inside of the constrained devices space it originally aimed for
and outside.
This document gives an overview of applications that
are, can, may,
and would better not be implemented on top of CoAP.

--- middle

# Introduction {#introduction}

\[ See abstract for now \]

# Applications

## Publish-Subscribe services

Publish-subscribe services (pubsub) are a widespread tool for the some of the fundamental use cases of Internet of Things (IoT) protocols:
acquiring sensor data and controlling actuators.

A pubsub implementation has been in development since shorlty after the original CoAP publication
and is as of now still in draft status, as {{?I-D.ietf-core-coap-pubsub}}.

Competing with

: MQTT

Strong points

: Once a topic is set up, data can be sent and received
  by CoAP clients that are not even aware of pubsub,
  as long as they can PUT or GET (possibly with observation) data to and from configured URIs.

Weak points

: To implement a pubsub broker that supports arbitrarily many topics,
  some (potentially difficult-to-implement) compromises have to be made.

## Remote configuration

The OMA LwM2M protocol
(which caters for several applications at the granularity of this document)
includes provisions for configuring and monitoring devices over the network,
setting properties such as a time server
and reading properties such as a network interface's packet count.

In parallel, the NETCONF protocol and its YANG modelling language
have been ported to the constrained ecosystem as CORECONF {{?I-D.ietf-core-comi}}.
By using numeric identifiers with good compression properties,
it can efficiently express data both from shared and from bespoke models in single requests.

Competing with

: SNMP \[ ? \], Puppet \[ ? \]

### Network status monitoring

Related to remote configuration,
CoAP is used as the signalling channel of DOTS ({{?RFC132}}).

Strong points

: CoAP over UDP/DTLS provides operational signalling on links under attack,
  on which a TCP/TLS based connection would fail.

: CoAP's consistency across transports
  makes it easy to adjust to situations in which UDP is uanvailable,
  sacrificing some properties but leaving the high-level protocol unmodified.

Weak points

: CoAP's default parameters for flow control (such as `PROBING_RATE`) are unsuitable for this application
  and need to be customized.

### Runtime configuration

Related to remote network configuration,
but used without human intervention,
CoAP is used negotiate cryptographic keys and other short-lived network configuration,
eg. in {{CoJP}}, {{ace-key-groupcomm-oscore}}, and {{OpenThread Multicast}}.
This is comparable to how {{DHCP}} or {{IGMP}} exchanges configuration.

Strong points

: When the exchanged communication is essential to joining the network in the first place,
  CoAP traffic exchanged over a temporary link can easily be proxied into the actual network,
  even when routing is not an option yet.

## Software updates

The SUIT manifest format {{?I-D.ietf-suit-manifest}} can be used to describe firmware updates
that can be performed over CoAP or any other protocol that is expressible in terms of URIs.

The OMA LwM2M protocol also contains provisions for firmware updates over CoAP.

## Network file system

Using CoAP as a backend for a no-frills file service
is a simple composition and is provided as a demo by the aiocoap library and a module in the RIOT operating system.

It has never been specified and described;
that gap is closed in {{coap-filesystem}}.

Competing with

: WebDAV, NFS, FTP

Strong points

: CoAP protocol already provides random read access (through the Block2 option),
  optimistic locking and cache (through the ETag and If-Match options)
  and change notification (through the Observe option).

: Files can be used in other CoAP protocols without the client's awareness (e.g. for SUIT)

Weak points

: Transfer of large files is inefficient due to the repetition of file names in block-wise requests
  (mitigated when using CoAP-over-TCP and BERT).

: Advanced file system functionality (file metadata, server-to-server copies, renaming, locking)
  would need additional specifications.

## Network address resolution

The Domain Name System (DNS) can be utilized from CoAP using the mechanisms described in {{?I-D.draft-lenders-dns-over-coap}}.

Strong points

: Savings in firmware complexity by using infrastructure shared with other applications.

: Potential for traffic (and thus energy) reduction by using request-response binding.

Weak points

: Not deployed in existing networks.

## Time service

A primitive time service can be assembled by creating a CoAP resource that returns the server's current time,
e.g., in a UNIX time stamp represented in decimal ASCII, or in CBOR using any of timestamp tags (0, 1 or 1001).

Such services have been in use since at least 2013,
and are easy to operate and scale.

There is a (currently long expired) document describing a lightweight authenticated time synchronization protocol
that is embedded into the ACE framework {{?RFC9200}} in {{?I-D.navas-ace-secure-time-synchronization}}
and typically used with CoAP.

Competing with

: SNTP, NTP, NTS

Strong points

: Savings in firmware complexity by using infrastructure shared with other applications.

: Compact messages.

: Reuse of existing security associations.

Weak points

: None of the advanced features of (S)NTP, such as distinction between receive and transmit timestamps.
  Not even leap seconds are advertised
  (but that can be mitigated by using a time scale that is not affected by them, such as TAI).

: Generally only suitable for the last hop in time synchronization.

: NTS supports key identifiers for stateless operations. {{?I-D.amsuess-core-stateless-oscore}} is explorative work to bring OSCORE to and above parity.

## Terminal access

Virtual terminal access was one of the first network applications described in an RFC ({{?RFC15}}),
and popular to date.

There is no full specification yet as to how to express the data streams
of character based user input
and character based text responses in CoAP.
Necessary components,
as well as optional future extensions,
have been sketched and implemented for the RIOT operating system at <https://forum.riot-os.org/t/coap-remote-shell/3340/5>
and implemented in [coap-scroll-ring-server](https://docs.rs/coap-scroll-ring-server/latest/coap_scroll_ring_server/struct.BufferHandler.html).
Unlike SSH,
that sketch assumes the presence of a single virtual terminal
(as opposed to one created per connection).
On platforms with dynamic resources and per-process output capture,
an SSH-like muliplexing can be created based on the resource collection pattern described in {{?I-D.ietf-core-interfaces}}.

Competing with

: SSH

Strong points

: The head-of-line blocking that occasionally plagues TCP based connections is eliminiated
  in favor of on-demand recovery
  (i.e., observing the last output will produce the latest chunk of output,
  and the terminal may recover skipped data later if it is still in the device's back-scroll buffer).

Weak points

: The default retransmission characteristics of CoAP make operations painfully slow when encountering packet loss.
  Tuning of parameters or the implementation of advanced flow control as described in {{?I-D.ietf-core-fasor}} are necessary for smooth operation.

: On-demand recovery is incompatible with regular terminals,
  and requires either fully managed terminals (where the full output is reprinted when lost fragments are recovered)
  or accepting the loss of data where printed exceeding the network speed.
  (Data is still lost gracefully, as the loss is detected and can be indicated visually).

## Chat services

The CoMatrix project <https://comatrix.eu/> has demonstrated that the Matrix chat protocol
can be simplified to the point where it becomes usable transparently with constrained devices.

## Web browsing

Competing with

: HTTP {{?RFC9110}} over its various transports; Gemini ({{gemini}}).

By virtue of cross proxying to HTTP,
CoAP is generally capable of transporting web pages the same way as HTTP,
albeit at a reduced feature set (in particular, most HTTP headers can not be expressed in CoAP).

CoAP offers only niche benefits over HTTP when combined with HTML,
the predominant markup language on the web:
Any benefits of a more compact transport or implementation are dwarved by the typical size of pages and the complexity of the HTML ecosystem.

CoAP might be a suitable transport for Small Web environments such as Gemini {{gemini}},
which can be rendered even by constrained devices.


## E-Mail

While E-Mail was part of the considerations that led to the definition of the Proxy-Uri option
(which would technically allow a cross-proxy to accept POST requests to, say, `mailto:office@example.com?subject=Sensor%20failure`),
no attempts are known to send or receive E-Mail over CoAP.

## Video streaming

The use of CoAP for real time video streaming and telemetry from Unmanned Aerial Vehicles (UAVs)
has been explored in {{?I-D.bhattacharyya-core-a-realist}}.

It is unclear whether CoAP could actually outperform unconstrained streaming protocols such as WebRTC,
or whether devices that produce and consume video benefit from the constraints of CoAP.

## Tunneling

Unlike HTTP,
CoAP does neither provide a dedicated method for encapsulating streaming or packet based network connections
(HTTP has a CONNECT method for streaming, {{Section 9.3.6 of RFC9110}})
nor has a means of encapsulating network traffic been specified
(HTTP has BOSH {{XEP-0124}} for streaming connections; VPNs such as OpenVPN can be operated over HTTP proxies {{OpenVPNproxy}}).
Early versions (up to -10) of {{?I-D.ietf-anima-constrained-join-proxy-10}} sketched a means of transporting UDP in a CoAP-like way.

However,
CoAP can aggregate exchanges with multiple peers
inside a single CoAP hop.
This can serve to protect the user's privacy
(by using (D)TLS or {{?I-D.tiloca-core-oscore-capable-proxies}} on the hop to the proxy, hiding which servers a client is communicating with from its local network)
and for efficiency reasons
(by using a single outgoing TCP connection from a device in a cellular network,
or even a more power optimized hop such as {{CoAP-over-NB-IoT}}
to keep observations active on multiple unrelated services).
This bears similarity with the approach of {{OHAI}},
but leverages CoAP's proxying mechanisms instead of using a gateway or relay resource.

## One-off experiments

CoAP has been used to provide location services (directions from a given point)
and calendar information (meeting agenda)
for events {{summit2023}}.

--- back

# CoAP File Service {#coap-filesystem}

This sketches \[ TBD: describes \] a file transfer protocol / remote file system built on top of CoAP.

A file server works similar to a WebDAV server, and follows these rules
(which are sometimes expressed from the point of view of the server,
but apply when a client maps them back into a file system in such a way that operations can round-trip):

* Directories are unconditionally represented by URIs with a trailing slash; files by those without one.

  The GET operation is used to list them (for there is no PROPFIND operation in CoAP).
  Different media types migth be used depending on the capabilities of the parties,
  with application/link-format as a base line.

  (Note that application/link-format is not particularly efficient for this purpose,
  as the directory listing needs to repeat the requested resource's full path for each entry).

  Clients need to be prepared for links that do not follow the regular pattern of a directory advertising its children,
  but (as with all unknown links) may ignore them.

  While the protocol does technically support having a file and a directory of the same name inside one location,
  their use is discouraged:
  While generic web clients can interact with both resources,
  most file systems backing the file server
  as well as clients that expose a file-system-like interface
  are often not able to create or access those.

* Paths are constructed by placing directory names and either an empty string (for the trailing slash) or the unescaped file name in Uri-Path options.

  This is easy to use with CRI references ({{?I-D.ietf-core-href}}),
  where a link from a directory to a file contained in it is expressed as `[1, ["name of the file"]]`.

  Expressed in terms of URIs, this automatically picks the unescaped option
  where URIs have a choice of options
  (e.g. to encode a file named "The 7:1 balun.txt"
  as either "./The%207:1%20balun.txt" or "./The%207%3A1%20balun.txt"):
  CoAP can only express the former style.
  (In CRIs, the latter can be expressed with the text-or-pet feature).
  This is consistent with {{Section 2.2 of ?RFC3986}}
  ("should percent-encode [...] unless these characters are specifically
  allowed by the URI scheme to represent data in that component")
  because along with the "coap" URI scheme, {{RFC7252}} describes its Uri-Path options (which form its paths) to contain "any character sequence".

  The protocol technically allows having a "/" (slash)  character as part of path names.
  While generic web clients can interact with such resources,
  most file systems backing the file server
  as well as clients that expose a file-system-like interface
  are often not able to create or access those.

* Clients may attempt to treat any URI composed of the file server entry URI and additional path segments as files on the file server.
  Consequently, any additional services the file server may provide
  (e.g., as resources specified in extensions)
  are necessarily assigned URIs with a query,
  for these can not be inadvertedly constructed in an attempt to access a file.

* For lack of a HEAD option,
  file metadata can only be obtained by performing a GET on the directory containing the file,
  or a FETCH for efficiency if suitable media types are defined.

  All metadata is provided on a best-effort basis,
  and the supported directory formats limit what can be expressed.
  Typical supported metadata are the media type (expressed as `ct` in link format)
  and the size (`sz`).

* If write support is available and permissions allow,
  a client can create files by performing a PUT operation on a previously nonexistent resource.

  Files can be overwritten by PUTting a new representation.
  Files sent with block-wise transfer should be processed atomically by the server
  unless explicitly negotiated otherwise.
  (On POSIX file systems,
  this can be implemented without additional state by storing the blocks in a temporary file
  whose name contains the original file name and the block key of the request,
  and renaming it to the target name when receiving the last block).

  Directories can be created with PUT as well
  (it is clear from their path that they are directories);
  these would typically be empty and not have a content format,
  but may use a content format that already describes directory metadata.
  (It is up to the client to fall back to plain directory creation,
  if that is a viable option for it -- it would be if it's just about unimportant metadata, but might not if the initial metadata indicates an ACL).

* Files and directories can be removed using the DELETE operation,
  subject ot the same conditions as writing to resources.

  Deleting directories is recursive;
  client that desire POSIX semantics need to check whether the directory is empty
  and use the If-Match option with the empty directory's ETag to avoid race conditions.

* Regular CoAP extensions apply if the parties support them, for example:

  * Observation can be used to watch files or directories for changes.

  * ETags (e.g. derived from the file's stats) can be used to ensure that large files are assembled correctly by the client,
    and to perform file updates with optimistic locking by using the If-Match option.

    Note that ETags are always "strong" in CoAP:
    They indicate that a precise representation of a resource has not changed.
    If a resource has different representations (eg. a directory listing may be available in different media types),
    they may have different ETags.
    They may also have identical ETags,
    but that means that if one format carries less information,
    its ETag needs to change when any representation changes,
    not just the selected one.

    Consequently,
    if a client learns of a resource's ETag
    and revalidates that that ETag is still valid,
    it can not know which properties of the resource have stayed constant.
    (In the extreme case, the representation might merely express that the resource exists).
    Only when knowing the representation,
    or at least relevant metadata of the tagged representation,
    can the client be sure that an ETag's validity has any meaning
    beyond that the resource still exists.

    For file services backed by content based naming systems (such as git),
    it is tempting to use content identifiers for ETags --
    consequently, a directory will be flagged as changed when any file below it changes.
    To allow monitoring directories for additions and removal of files,
    it is RECOMMENDED that a representation is provided that does not contain the contained files' and directories' ETags,
    and has a stable ETag that is distinct from any representation that does include them.
    There is no recommendation as to whether the default representation
    should contain ETags or not.

* Additional features can be specified and advertised separately,
  either per resoource or by a named specification that provides templates for further operations.

  For example,
  a specification might say that when a file system is advertised with a given interface (`if` parameter of link format),
  for each file and directory there is an associated resource at `?acl` that describes access control applicable to that file,
  and can be used with GET and PUT as per the ACL's policies.

  Additional operations can use their custom media types and methods,
  and possibly create more resources.
  For example,
  a server-to-server copy (again, advertised by a suitable interface parameter)
  could provide a `?copy` resource under any directory,
  to which a CBOR list containing two CRIs (source and destination) would be posted.
  That specification might describe that if copies are not completed instantly,
  the response might indicate a new location using Uri-Path and Uri-Query options
  (the latter might be necessary to not conflict with existing files)
  which tracks the status of the operation.

* File service is compatible with "index.html" style directory indices provided statically in the file system.
  It is recommended to not serve index files of typical directory listing formats (such as application/link-format)
  as these might mislead the client about the file system's content,
  and prevent clients that access the server as a file server from even seeing the providing file's name.

  These files still need to be written to or deleted in their file version (e.g. as "index.html");
  the file server may use yet to be specified link relations to point out which files are being served as the index files.

Some implementation guidance should be provided around the interaction between idempotent requests that have no actual effect and preconditions:
If a DELETE with If-Match is transmitted again on a new token (by a proxy relying on its idempotence),
should the server respond Deleted rather than Not Found?
If a PUT with If-Match is transmitted again after it has been acted on,
should the server respond Changed rather than Precondition Failed?
(Probably "No" to both, as the former is easily recognizable by the client,
and the latter would delay faulting by long,
but still needs further thought.)

# Change log

From -05 to -06:

* Reference use of CoAP for meeting calendars and routing.
* Details for comparison to NTS.
* Link to implementation of terminal.

From -04 to -05:

* (Properly) reference ace-secure-time-synchronization
* Update references
* Minor editorial fixes

From -03 to -04:

* Add section about tunneling
* Extend notes on theuse of ETags on file servers

From -02 to -03:

* Added Runtime configuration section
* Terminal access now has an implementation

From -01 to -02:

* Added "Web browsing" section

From -00 to -01:

* Added details to file service
