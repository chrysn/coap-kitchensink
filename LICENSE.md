# License

See the
[guidelines for contributions](https://gitlab.com/chrysn/coap-kitchensink/-/blob/main/CONTRIBUTING.md).
